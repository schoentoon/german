A small set of tools that were mostly written to better understand some of the rules of the German language.
It is in no way meant to give an accurate result for any input.
It was mostly written in order for me to better and faster pick up on the language rules.
Most of this is directly based on https://www.bol.com/nl/p/duits-voor-zelfstudie-prisma-taalcursus/9200000059203270/