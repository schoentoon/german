package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/schoentoon/german/lib"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("You may want to provide some actual input")
		os.Exit(1)
	}

	for _, arg := range os.Args[1:] {
		split := strings.Split(arg, " ")
		article := lib.Das
		word := arg
		if len(split) == 1 {
			article, _ = lib.GuessArticle(arg)
		} else {
			a := strings.ToLower(split[0])
			if a == "der" {
				article = lib.Der
			} else if a == "die" {
				article = lib.Die
			}
			word = split[1]
		}
		fmt.Printf("%s %s - ", article, word)

		pluralGuess, note := lib.GuessPlural(article, word)
		fmt.Printf("die %s\n%s\n\n", pluralGuess, note)
	}

	fmt.Println("Do keep in mind that there can always be exceptions!")
}
