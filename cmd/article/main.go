package main

import (
	"fmt"
	"os"

	"gitlab.com/schoentoon/german/lib"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("You may want to provide some actual input")
		os.Exit(1)
	}

	for _, arg := range os.Args[1:] {
		guess, msg := lib.GuessArticle(arg)
		fmt.Printf("%s %s\n", guess, arg)
		fmt.Printf("%s\n", msg)
	}

	fmt.Println("Do keep in mind that there can always be exceptions!")
}
