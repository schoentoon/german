package main

import (
	"fmt"
	"io"
	"strings"

	"gitlab.com/schoentoon/german/lib"

	"github.com/c-bata/go-prompt"
)

type PluralGuesser struct {
}

func (a *PluralGuesser) Name() string {
	return "plural"
}

func (a *PluralGuesser) Description() string {
	return "Guesses plural form of a word"
}

func (a *PluralGuesser) Autocomplete(app *Application, in prompt.Document) []prompt.Suggest {
	return []prompt.Suggest{}
}

func (a *PluralGuesser) Execute(app *Application, args string, out io.Writer) error {
	// GuessArticle could panic, let's handle this gracefully
	defer func() {
		r := recover()
		if r != nil {
			fmt.Fprintf(out, "%s\n", r)
		}
	}()

	split := strings.Split(args, " ")
	article := lib.Das
	word := args
	if len(split) == 1 {
		article, _ = lib.GuessArticle(args)
		fmt.Fprintf(out, "Guessed the article \"%s\"\n", article)
	} else {
		a := strings.ToLower(split[0])
		if a == "der" {
			article = lib.Der
		} else if a == "die" {
			article = lib.Die
		}
		word = split[1]
	}

	pluralGuess, note := lib.GuessPlural(article, word)
	fmt.Fprintf(out, "\ndie %s\n\n", pluralGuess)
	fmt.Fprintf(out, "%s\n", note)
	return nil
}
