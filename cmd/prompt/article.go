package main

import (
	"fmt"
	"io"

	"gitlab.com/schoentoon/german/lib"

	"github.com/c-bata/go-prompt"
)

type ArticleGuesser struct {
}

func (a *ArticleGuesser) Name() string {
	return "article"
}

func (a *ArticleGuesser) Description() string {
	return "Guesses the article based on the general rules for this, exceptions may apply"
}

func (a *ArticleGuesser) Autocomplete(app *Application, in prompt.Document) []prompt.Suggest {
	return []prompt.Suggest{}
}

func (a *ArticleGuesser) Execute(app *Application, args string, out io.Writer) error {
	// GuessArticle could panic, let's handle this gracefully
	defer func() {
		r := recover()
		if r != nil {
			fmt.Fprintf(out, "%s\n", r)
		}
	}()

	guess, msg := lib.GuessArticle(args)
	fmt.Fprintf(out, "\n%s %s\n\n", guess, args)
	fmt.Fprintf(out, "%s\n", msg)
	return nil
}
