package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/c-bata/go-prompt"
	"github.com/c-bata/go-prompt/completer"
)

type Command interface {
	Name() string
	Description() string
	Autocomplete(app *Application, in prompt.Document) []prompt.Suggest
	Execute(app *Application, args string, out io.Writer) error
}

type Application struct {
	Commands []Command
}

func (a *Application) completer(in prompt.Document) []prompt.Suggest {
	w := in.GetWordBeforeCursor()
	line := in.TextBeforeCursor()
	blocks := strings.SplitN(line, " ", 2)
	cmd := blocks[0]

	for _, c := range a.Commands {
		if cmd == c.Name() {
			return c.Autocomplete(a, in)
		}
	}

	if len(blocks) != 1 || line == "" {
		return []prompt.Suggest{}
	}

	out := make([]prompt.Suggest, len(a.Commands))
	for i, c := range a.Commands {
		out[i] = prompt.Suggest{Text: c.Name(), Description: c.Description()}
	}
	return prompt.FilterHasPrefix(out, w, true)
}

func (a *Application) executor(in string) {
	in = strings.TrimSpace(in)

	blocks := strings.SplitN(in, " ", 2)
	if blocks[0] == "" {
		return
	}

	var cmd Command
	for _, c := range a.Commands {
		if c.Name() == blocks[0] {
			cmd = c
			break
		}
	}

	if cmd == nil {
		fmt.Println("Invalid command")
		return
	}

	out := os.Stdout

	var err error
	if len(blocks) == 1 {
		err = cmd.Execute(a, "", out)
	} else {
		err = cmd.Execute(a, blocks[1], out)
	}
	if err == flag.ErrHelp {
		return
	}
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}

func main() {
	a := Application{
		Commands: []Command{
			&ArticleGuesser{},
			&PluralGuesser{},
		},
	}

	if flag.NArg() > 0 {
		a.executor(strings.Join(flag.Args(), " "))
		return
	}

	p := prompt.New(
		a.executor,
		a.completer,
		prompt.OptionCompletionWordSeparator(completer.FilePathCompletionSeparator),
	)

	p.Run()
}
