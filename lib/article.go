package lib

import (
	"fmt"
	"strings"
)

type Article int

const (
	Der Article = iota
	Die
	Das
)

func (a Article) String() string {
	return [...]string{"Der", "Die", "Das"}[a]
}

// GuessArticle this will return the guessed Article, and a short message explaining why this was guessed
// and what you may have to look out for. This function could panic in case we run out of rules to check against
// in this case you're probably best off just looking up this word in a wordbook.
func GuessArticle(in string) (Article, string) {
	// ideally I would match it the order der/die/das, but I decided to match for das first as several
	// suffix checks would collide with der. "lein" for example would also match with the "in" check for die

	// a special case for "chen" as it isn't just a simple suffix check there, see the message that is returned
	if strings.HasSuffix(in, "chen") {
		return Der, fmt.Sprintf("WARNING! If \"chen\" is used here to make a word smaller, like Mädchen, Kätzchen, etc. The article would be *Das* instead.\n" +
			"Now we guessed Der instead due to it ending with \"en\".")
	}

	otherSuffix := [...]string{"lein", "o", "em", "um", "ment", "nis", "at", "ut"}
	for _, suffix := range otherSuffix {
		if strings.HasSuffix(in, suffix) {
			return Das, fmt.Sprintf("Word ends with \"%s\"", suffix)
		}
	}

	maleSuffix := [...]string{"er", "en", "el", "ling", "ig", "or", "ant", "ent", "ist", "ismus"}
	for _, suffix := range maleSuffix {
		if strings.HasSuffix(in, suffix) {
			return Der, fmt.Sprintf("Word ends with \"%s\"", suffix)
		}
	}

	femaleSuffix := [...]string{"eit", "ung", "schaft", "enz", "anz", "in", "ei", "ie", "ion", "ität", "ik", "ur", "a", "e"}
	for _, suffix := range femaleSuffix {
		if strings.HasSuffix(in, suffix) {
			return Die, fmt.Sprintf("Word ends with \"%s\"", suffix)
		}
	}

	panic("None of the rules matched, should probably look up this word.")
}
