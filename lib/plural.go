package lib

import (
	"fmt"
	"regexp"
	"strings"
)

func handleDerPlural(in string) (out, note string) {
	if strings.HasSuffix(in, "er") ||
		strings.HasSuffix(in, "en") ||
		strings.HasSuffix(in, "el") {
		return in, "Because the word ends with 'er', 'en' or 'el' the plural form is the same"
	}

	if strings.HasSuffix(in, "e") ||
		strings.HasSuffix(in, "or") ||
		strings.HasSuffix(in, "ant") ||
		strings.HasSuffix(in, "ent") {
		note := "Because the word ends with 'e', 'or', 'ant' or 'ent' the plural form is 'en'"
		if strings.HasSuffix(in, "e") {
			return fmt.Sprintf("%sn", in), note
		}
		return fmt.Sprintf("%sen", in), note
	}

	return fmt.Sprintf("%se", in), "There's a chance you'll have to add an umlaut to one of the vowels"
}

func handleDiePlural(in string) (out, note string) {
	if strings.HasSuffix(in, "e") {
		return fmt.Sprintf("%sn", in),
			"With die we simply have to add 'en' at the end, or just an 'n' if the word already ends with an 'e'"
	}
	if strings.HasSuffix(in, "in") {
		return fmt.Sprintf("%snen", in),
			"As this word ended with 'in' we add 'nen' instead of the regular 'en'"
	}
	return fmt.Sprintf("%sen", in),
		"We simply add 'en' at the end"
}

func handleDasPlural(in string) (out, note string) {
	// a simple map of vowels and their versions with umlauts, mainly to make the replacement as painless as possible
	// TODO: this map should probably be longer than it is right now
	umlautMap := map[string]string{
		"au": "äu",
		"ie": "ie",
		"a":  "ä",
		"e":  "ë",
		"i":  "i",
		"o":  "ö",
		"u":  "ü",
		"ä":  "ä",
		"ë":  "ë",
		"ö":  "ö",
		"ü":  "ü",
	}
	groupRegex := regexp.MustCompile("(au|ie|a|e|i|o|u|ä|ë|ö|ü)")
	result := groupRegex.FindAllString(in, -1)
	if len(result) == 1 {
		// as we only seem to have one lettergroup in this word (just a single vowel in the word)
		// the plural will be an umlaut on the first vowel and er
		return fmt.Sprintf("%ser", strings.Replace(in, result[0], umlautMap[result[0]], 1)),
			"As we have a single lettergroup we add an umlaut and we add er at the end"
	}

	if !strings.HasSuffix(in, "e") {
		return fmt.Sprintf("%se", in),
			"The regular form with das is adding an 'e' at the end, do keep in mind that there's the exception for 'strange' words (mostly from other languages) where you'll have to add an 's' at the end instead."
	}

	return in, note
}

func GuessPlural(article Article, in string) (out, note string) {
	switch article {
	case Der:
		return handleDerPlural(in)
	case Die:
		return handleDiePlural(in)
	case Das:
		return handleDasPlural(in)
	}
	panic("We should be able to reach this :/")
}
