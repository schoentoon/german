package lib

import "testing"

func TestPlural(t *testing.T) {
	cases := []struct {
		article Article
		input   string
		output  string
	}{
		{Der, "Brief", "Briefe"},
		{Der, "Transport", "Transporte"},
		{Der, "Tag", "Tage"},
		{Der, "Computer", "Computer"},
		{Der, "Lehrer", "Lehrer"},
		{Der, "Artikel", "Artikel"},
		{Der, "Schlüssel", "Schlüssel"},
		{Der, "Laden", "Laden"},
		{Der, "Wagen", "Wagen"},
		{Der, "Name", "Namen"},
		{Der, "Kunde", "Kunden"},
		{Der, "Direktor", "Direktoren"},
		{Der, "Faktor", "Faktoren"},
		{Der, "Referent", "Referenten"},
		{Der, "Lieferant", "Lieferanten"},
		{Das, "Angebot", "Angebote"},
		{Das, "Telefon", "Telefone"},
		{Das, "Ergebnis", "Ergebnise"},
		{Das, "Produkt", "Produkte"},
		{Das, "Geschäft", "Geschäfte"},
		{Das, "Buch", "Bücher"},
		{Das, "Land", "Länder"},
		{Das, "Wort", "Wörter"},
		{Das, "Haus", "Häuser"},
		{Das, "Kind", "Kinder"},
		{Das, "Blatt", "Blätter"},
		{Die, "Frau", "Frauen"},
		{Die, "Tür", "Türen"},
		//{Die, "Firma", "Firmen"}, // you lying book, it's not just adding '-en' at the end if I have to get rid of that final a as well
		{Die, "Versammlung", "Versammlungen"},
		{Die, "Woche", "Wochen"},
		{Die, "Kollegin", "Kolleginnen"},
		{Die, "Konferenz", "Konferenzen"},
	}

	for _, test := range cases {
		guess, _ := GuessPlural(test.article, test.input)
		if guess != test.output {
			t.Errorf("Test case %s reported as %s, but should be %s", test.input, guess, test.output)
		}
	}
}
